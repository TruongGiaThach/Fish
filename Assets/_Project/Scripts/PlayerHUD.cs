﻿using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Shmup
{
    public class PlayerHUD : MonoBehaviour
    {
        [SerializeField] Image healthBar;
        [SerializeField] Image staminaBar;
        [SerializeField] Image shieldBar;
        [SerializeField] TextMeshProUGUI scoreText;
        [SerializeField] TextMeshProUGUI speedText;
        [SerializeField] TextMeshProUGUI coinText;
        [SerializeField] Image bloodImage;
        public Color targetColor;

        private float playerSpeed;
        private int playerCoin;
        private int playerShield;

        void Start()
        {
            playerSpeed = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GetPlayerSpeed();
            playerCoin = 0;
            playerShield = 0;
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
            this.RegisterListener(EventID.OnCoinCollect, (param) => OnPlayerCoinChange((int)param));
            this.RegisterListener(EventID.OnShieldEnable, (param) => OnPlayerShieldEnable((bool)param));
            this.RegisterListener(EventID.OnShieldChange, (param) => OnPlayerShieldChange((int)param));
            this.RegisterListener(EventID.TriggerFlashScreen, (param) => TriggerFlashScreen((bool)param));

        }
        void TriggerFlashScreen(bool isFlash)
        {
            if (isFlash)
            {
                bloodImage.gameObject.SetActive(true);
                ScreenFlash();
            }
            else
            {
                StopCoroutine("PlayAnimation");
                bloodImage.gameObject.SetActive(false);
            }
        }
        void OnPlayerSpeedChange(float speed)
        {
            playerSpeed = speed;
        }
        void OnPlayerCoinChange(int amount)
        {
            playerCoin = amount;
        }
        void OnPlayerShieldEnable(bool isEnable)
        {
            if (!isEnable)
            {
                playerShield = 0;
            }
        }
        void OnPlayerShieldChange(int amount)
        {
            playerShield = amount;
        }
        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnCoinCollect, (param) => OnPlayerCoinChange((int)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnShieldEnable, (param) => OnPlayerShieldEnable((bool)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnShieldChange, (param) => OnPlayerShieldChange((int)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.TriggerFlashScreen, (param) => TriggerFlashScreen((bool)param));
        }


        void Update()
        {
            healthBar.fillAmount = GameManager.Instance.Player.GetHealthNormalized();
            staminaBar.fillAmount = GameManager.Instance.Player.GetStaminaNormalized();
            shieldBar.fillAmount = GameManager.Instance.Player.GetShieldNormalized();
            scoreText.text = $"Score: {GameManager.Instance.GetScore()}";
            speedText.text = $"{playerSpeed} km/h";
            coinText.text = $"$ {playerCoin}";
        }

        void ScreenFlash()
        {
            StartCoroutine(PlayAnimation());
        }
        IEnumerator PlayAnimation()
        {
            while (true)
            {
                LeanTween.value(bloodImage.gameObject, 0.4f, 0.0f, 3f)
                .setOnUpdate((float val) =>
                {
                    if (bloodImage != null)
                    {
                        bloodImage.color = new Color(targetColor.r, targetColor.g, targetColor.b, val);
                    }
                })
                .setOnComplete(() =>
                {
                    bloodImage.gameObject.SetActive(false);
                });
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
}