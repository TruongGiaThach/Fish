﻿using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Utilities;

namespace Shmup.Assets._Project.Scripts.Item
{
    public class PyramidCoinSpawner : MonoBehaviour, SpawnerInterface
    {
        [SerializeField] GameObject itemPrefabs;
        GameObject item;

        float itemSizeWidth;
        float itemSizeHeigh;
        [SerializeField] Camera _camera;
        Pooler itemPooler = null;

        private void Start()
        {
            Vector3 max = itemPrefabs.GetComponent<Renderer>().bounds.max;
            Vector3 min = itemPrefabs.GetComponent<Renderer>().bounds.min;
            itemSizeWidth = Mathf.Abs(max.x - min.x);
            itemSizeHeigh = Mathf.Abs(max.y - min.y);
            itemPooler = new Pooler(itemPrefabs, 10);
        }
        public void Free(GameObject itemPrefabs)
        {
            itemPooler.Free(itemPrefabs);
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnWallSpawn, (param) => OnWallSpawn((GameObject)param));
        }
        void OnWallSpawn(GameObject wall)
        {
            Vector3 max = wall.GetComponent<Renderer>().bounds.max;
            max.x += itemSizeWidth;
            Vector3 min = wall.GetComponent<Renderer>().bounds.min;
            min.x = max.x;
            for (int i = 0; i < 3; i++)
            {
                var item = itemPooler.Get();
                min.x += itemSizeWidth * i;
                item.transform.position = min.With(z: -0.1f);
                item.GetComponent<DestroyOutOfBoundsX>().SetCamera(_camera);
                item.GetComponent<DestroyOutOfBoundsX>().SetSpawner(this);
            }
        }

        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnWallSpawn, (param) => OnWallSpawn((GameObject)param));
        }


    }
}
