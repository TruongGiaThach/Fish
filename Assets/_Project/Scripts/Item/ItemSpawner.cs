﻿using System.Collections.Generic;
using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Shmup {
    public class ItemSpawner : MonoBehaviour, SpawnerInterface {
        [SerializeField] GameObject itemPrefabs;
        [SerializeField] float spawnInterval = 3f;
        [SerializeField] float spawnRadius = 3f;
        [SerializeField] Camera _camera;
        CoroutineHandle spawnCoroutine;
        Pooler itemPooler = null;
        private void Awake()
        {
            if (itemPooler == null)
            {
                itemPooler = new Pooler(itemPrefabs, 10);
            }
        }
       
        public void Free ( GameObject itemPrefabs )
        {
            itemPooler.Free( itemPrefabs );
        }

        void Start() => spawnCoroutine = Timing.RunCoroutine(SpawnItems());

        void OnDestroy()
        {
            if (spawnCoroutine != null)
            {

                Timing.KillCoroutines(spawnCoroutine);
            }
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnFinishStageTrigger, (param) => OnFinishStageTrigger((FinishStageObjectTransfer)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
            this.RegisterListener(EventID.OnFinishStageTrigger, (param) => OnFinishStageTrigger((FinishStageObjectTransfer)param));

        }
        void OnEnterEndLevel(GameObject player)
        {
            if (player != null)
            {
                Timing.KillCoroutines(spawnCoroutine);
            }
        }
        void OnFinishStageTrigger(FinishStageObjectTransfer player)
        {
            Timing.KillCoroutines(spawnCoroutine);
        }

        IEnumerator<float> SpawnItems() {
            while (true) {
                yield return Timing.WaitForSeconds(spawnInterval);
                var item = itemPooler.Get();
                item.GetComponent<DestroyOutOfBoundsX>().SetCamera(_camera);
                item.GetComponent<DestroyOutOfBoundsX>().SetSpawner(this);
                item.transform.position = (transform.position + Random.insideUnitSphere).With(z: -0.1f) * spawnRadius;
            }
        }
    }
}