﻿using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Shmup
{
    public class HeheSpawner : MonoBehaviour, SpawnerInterface
    {
        [SerializeField] GameObject itemPrefabs;
        [SerializeField] GameObject flagPrefabs;
        [SerializeField] float spawnInterval = 3f;
        [SerializeField] float spawnRadius = 3f;
        [SerializeField] Camera _camera;
        [SerializeField] float spawnDistance = 5;
        Pooler itemPooler = null;
        GameObject flag;
        private void Awake()
        {
            if (itemPooler == null)
            {
                itemPooler = new Pooler(itemPrefabs, 5);
            }
        }
        private void Start()
        {
            flag = Instantiate(flagPrefabs);
            flag.SetActive(false);
            //StartCoroutine("SpawnItems");

        }
        public void Free(GameObject itemPrefabs)
        {
            itemPooler.Free(itemPrefabs);
        }
        void OnDestroy()
        {

            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnFinishStageTrigger, (param) => OnFinishStageTrigger((FinishStageObjectTransfer)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.TriggerHeheEnemy, (param) => TriggerHeheEnemy((GameObject)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
            this.RegisterListener(EventID.TriggerHeheEnemy, (param) => TriggerHeheEnemy((GameObject)param));
            this.RegisterListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
            this.RegisterListener(EventID.OnFinishStageTrigger, (param) => OnFinishStageTrigger((FinishStageObjectTransfer)param));
        }
        void OnEnterEndLevel(GameObject player)
        {
            StopCoroutine("SpawnItems");
        }
        void OnFinishStageTrigger(FinishStageObjectTransfer player)
        {
            StopCoroutine("SpawnItems");
        }
        void OnPlayerSpeedChange(float speed)
        {
            spawnInterval /= speed;
        }
        void TriggerHeheEnemy(GameObject player)
        {
            StartCoroutine(SpawnItems());
        }
        IEnumerator SpawnItems()
        {

            yield return new WaitForSeconds(2);
            bool isFromBottom =
                Random.Range(0, 2) > 0;
            flag.SetActive(true);
            this.PostEvent(EventID.TriggerFlashScreen, true);
            Vector3 spawnPosition = (transform.position + Random.insideUnitSphere).With(z: -0.5f) * spawnRadius;
            spawnPosition.y = _camera.ScreenToWorldPoint(new Vector3(0, 0, _camera.nearClipPlane)).y + 1.5f;
            flag.transform.position = spawnPosition;


            yield return new WaitForSeconds(spawnInterval);
            this.PostEvent(EventID.TriggerFlashScreen, false);
            flag.SetActive(false);
            var item = itemPooler.Get();
            item.GetComponent<DestroyOutOfBoundsX>().SetCamera(_camera);
            item.GetComponent<DestroyOutOfBoundsX>().SetSpawner(this);
            item.transform.position = spawnPosition;
            item.transform.eulerAngles = new Vector3(
                item.transform.eulerAngles.x,
                item.transform.eulerAngles.y,
                item.transform.eulerAngles.z + 90
            //((isFromBottom) ? 90 : -90)
            );

        }
        IEnumerator CorotineSpawnItems()
        {
            while (true)
            {
                SpawnItems();
            }

        }
    }
}