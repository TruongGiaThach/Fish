﻿using System.Collections.Generic;
using System.Drawing;
using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Shmup
{
    public class CoinSpawner : MonoBehaviour, SpawnerInterface
    {
        [SerializeField] GameObject itemPrefabs;
        GameObject item;

        float itemSizeWidth;
        float itemSizeHeigh;
        [SerializeField] Camera _camera;
        Pooler itemPooler = null;

        private void Start()
        {
            Vector3 max = itemPrefabs.GetComponent<Renderer>().bounds.max;
            Vector3 min = itemPrefabs.GetComponent<Renderer>().bounds.min;
            itemSizeWidth = Mathf.Abs(max.x - min.x);
            itemSizeHeigh = Mathf.Abs(max.y - min.y);
            itemPooler = new Pooler(itemPrefabs, 10);
        }
        public void Free(GameObject itemPrefabs)
        {
            itemPooler.Free(itemPrefabs);
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnWallSpawn, (param) => OnWallSpawn((GameObject)param));
        }
        void OnWallSpawn(GameObject wall)
        {
            Vector3 max = wall.GetComponent<Renderer>().bounds.max;
            max.x += itemSizeWidth;
            Vector3 min = wall.GetComponent<Renderer>().bounds.min;
            min.x = max.x + 3;
            min.y += itemSizeHeigh;
            for (int i = 0; i < 3; i++)
            {
                min.x += itemSizeWidth * i;

                var item = itemPooler.Get();
                item.transform.position = min.With(y: min.y * i, z: -0.1f);
                item.GetComponent<DestroyOutOfBoundsX>().SetCamera(_camera);
                item.GetComponent<DestroyOutOfBoundsX>().SetSpawner(this);

            }
        }

        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnWallSpawn, (param) => OnWallSpawn((GameObject)param));
        }


    }

}