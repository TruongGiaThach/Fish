﻿using System.Collections.Generic;
using System.Drawing;
using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Shmup
{
    public class ShieldSpawner : MonoBehaviour, SpawnerInterface
    {
        [SerializeField] GameObject itemPrefabs;
        GameObject item;

        float itemSizeWidth;
        float itemSizeHeigh;
        [SerializeField] Camera _camera;
        Pooler itemPooler = null;

        private void Start()
        {
            Vector3 max = itemPrefabs.GetComponentInChildren<Renderer>().bounds.max;
            Vector3 min = itemPrefabs.GetComponentInChildren<Renderer>().bounds.min;
            itemSizeWidth = Mathf.Abs(max.x - min.x);
            itemSizeHeigh = Mathf.Abs(max.y - min.y);
            itemPooler = new Pooler(itemPrefabs, 10);
        }
        public void Free(GameObject itemPrefabs)
        {
            itemPooler.Free(itemPrefabs);
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.TriggerHeheEnemy, (param) => TriggerHeheEnemy((GameObject)param));
        }
        void TriggerHeheEnemy(GameObject _gameObject)
        {
            Player player = _gameObject.GetComponent<Player>();
            var item = itemPooler.Get();
            Vector3 spawnPosition = (player.transform.position + Random.insideUnitSphere).With(x: player.transform.position.x + 3, z: -0.1f) ;
            item.transform.position = spawnPosition;
            item.GetComponent<DestroyOutOfBoundsX>().SetCamera(_camera);
            item.GetComponent<DestroyOutOfBoundsX>().SetSpawner(this);
        }

        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.TriggerHeheEnemy, (param) => TriggerHeheEnemy((GameObject)param));
        }


    }
}