﻿using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;

namespace Shmup {
    public class StaminaItem : Item {
        void OnTriggerEnter(Collider other) {
            
            other.GetComponent<Player>().AddStamina((int) amount);
            Destroy(gameObject);
        }
    }
}