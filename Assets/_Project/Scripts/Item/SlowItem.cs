﻿using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections;
using UnityEngine;
using Utilities;

namespace Shmup {
    public class SlowItem : Item {

        [SerializeField] int duration = 5;
        void OnTriggerEnter(Collider other)
        {
            this.PostEvent(EventID.OnSlowDebuffCollect,  duration);
            PlayerController player = other.GetComponent<PlayerController>();
            StartCoroutine(AdjustSpeed(player));
        }
        IEnumerator AdjustSpeed(PlayerController player)
        {
            transform.position = transform.position.With(z: 20);
            player.AddPlayerSpeed(-amount);
            player.AddPlayerJumpSpeed(-amount* 300);
            yield return new WaitForSeconds(duration);
            player.AddPlayerSpeed(amount);
            player.AddPlayerJumpSpeed(amount * 300);
            Destroy(gameObject);
        }
    }
}