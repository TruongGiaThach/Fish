﻿using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;

using System.Collections;
using UnityEngine;

namespace Shmup
{
    public class ShieldItem : Item
    {
        [SerializeField] float duration = 1f;

        private void OnTriggerEnter(Collider collision)
        {
            this.PostEvent(EventID.OnShieldItemCollect, collision);
            Player player = collision.GetComponent<Player>();
            player.AddShield((int)amount);
            Destroy(gameObject);
        }


    }
}