﻿using System.Collections;
using UnityEngine;

namespace Shmup
{
    public class Coin : Item
    {
        [SerializeField] GameObject explosionPrefab;

        private void OnTriggerEnter(Collider collision)
        {
            GameManager.Instance.AddCoin(10);
            if (explosionPrefab != null)
            {
                Vector3 contact = collision.transform.position;
                var hitVFX = Instantiate(explosionPrefab, new Vector3(contact.x + (1 * Time.deltaTime), contact.y, 0), Quaternion.identity);
                DestroyParticleSystem(hitVFX);
            }
            Destroy(gameObject);
        }
        void DestroyParticleSystem(GameObject vfx)
        {
            var ps = vfx.GetComponent<ParticleSystem>();
            if (ps == null)
            {
                ps = vfx.GetComponentInChildren<ParticleSystem>();
            }
            Destroy(vfx, ps.main.duration);
        }

    }
}