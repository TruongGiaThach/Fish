﻿using UnityEngine;
using Utilities;

namespace Shmup {
    public abstract class Item : MonoBehaviour {
        [SerializeField] protected float amount = 10f;
        [SerializeField, Layer] protected int layer = 9;

    }
}