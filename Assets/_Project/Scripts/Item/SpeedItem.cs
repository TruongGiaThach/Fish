﻿using System.Collections;
using UnityEngine;

namespace Shmup
{
    public class SpeedItem : Item
    {
        [SerializeField] float duration = 1f;
        void OnTriggerEnter(Collider other)
        {
            PlayerController player = other.GetComponent<PlayerController>();
            StartCoroutine(AdjustSpeed(player));
        }
        IEnumerator AdjustSpeed(PlayerController player)
        {
            player.AddPlayerSpeed(amount);
            yield return new WaitForSeconds(duration);
            player.AddPlayerSpeed(-amount);
            Destroy(gameObject);
        }
    }
}