﻿using System.Collections.Generic;
using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Shmup
{
    public class WallSpawner : MonoBehaviour, SpawnerInterface
    {
        [SerializeField] GameObject itemPrefab;
        [SerializeField] float spawnDistance = 5f;
        [SerializeField] float maxHeight;
        [SerializeField] float minHeight;
        GameObject item;
        Pooler itemPooler;
        float playerSpeed;
        float cameraSpeed;

        CoroutineHandle spawnCoroutine;
        [SerializeField] Camera _camera;

        private void Awake()
        {
            if (itemPooler == null)
            {
                itemPooler = new Pooler(itemPrefab, 10);
            }
            playerSpeed = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().GetPlayerSpeed();
            cameraSpeed = _camera.GetComponent<CameraController>().GetSpeed();
        }

        
        public void Free(GameObject item)
        {
            itemPooler.Free(item);
        }

        void Start() => spawnCoroutine = Timing.RunCoroutine(SpawnItems());

        void OnDestroy()
        {
            if (spawnCoroutine != null)
            {
                Timing.KillCoroutines(spawnCoroutine);
            }
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnFinishStageTrigger, (param) => OnFinishStageTrigger((FinishStageObjectTransfer)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnCameraSpeedChange, (param) => OnCameraSpeedChange((float)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));

        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
            this.RegisterListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
            this.RegisterListener(EventID.OnCameraSpeedChange, (param) => OnCameraSpeedChange((float)param));
            this.RegisterListener(EventID.OnFinishStageTrigger, (param) => OnFinishStageTrigger((FinishStageObjectTransfer)param));

        }
        void OnPlayerSpeedChange(float speed)
        {
            playerSpeed = speed;
        }
        void OnCameraSpeedChange(float speed)
        {
            playerSpeed = speed;
        }
        void OnEnterEndLevel(GameObject player)
        {
            if (player != null)
            {
                Timing.KillCoroutines(spawnCoroutine);
            }
        }
        void OnFinishStageTrigger(FinishStageObjectTransfer player)
        {
            Timing.KillCoroutines(spawnCoroutine);
        }
        IEnumerator<float> SpawnItems()
        {
            while (true)
            {
                yield return Timing.WaitForSeconds(spawnDistance / cameraSpeed);
                var item = itemPooler.Get();
                item.transform.position =
                    transform.position + new Vector3(0, Random.Range(minHeight, maxHeight), 10);
                item.GetComponent<DestroyOutOfBoundsX>().SetCamera(_camera);
                item.GetComponent<DestroyOutOfBoundsX>().SetSpawner(this);
                this.PostEvent(Assets._Project.Scripts.Models.EventID.OnWallSpawn, item);
            }
        }
    }
}