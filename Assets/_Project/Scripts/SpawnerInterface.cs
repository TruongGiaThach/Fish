using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    public interface SpawnerInterface
    {
        public void Free(GameObject gameObject);

    }
}
