using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    [RequireComponent(typeof(Rigidbody))]
    public class MoveForward : MonoBehaviour
    {
        private Transform m_Rigidbody;
        [SerializeField] private float speed;
        // Update is called once per frame
        private void Awake()
        {
            m_Rigidbody = this.GetComponent<Rigidbody>().transform;
        }
        void Update()
        {
            transform.position += Vector3.up * Time.deltaTime * speed;
        }
    }
}
