﻿using Eflatun.SceneReference;
using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System;
using System.Collections;
using UnityEngine;

namespace Shmup
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] SceneReference mainMenuScene;
        [SerializeField] SceneReference currentScene;
        [SerializeField] SceneReference nextScene;
        [SerializeField] GameObject gameOverUI;
        [SerializeField] GameObject gameFinishUI;
        private float startTime;
        private float elapsedTime;
        bool isFinishGame;

        public static GameManager Instance { get; private set; }
        public Player Player => player;

        Player player;
        PlayerController playerController;
        //Boss boss;
        float score;
        int stageCoin;
        float restartTimer = 2f;

        public bool IsGameOver() => player.GetHealthNormalized() <= 0 || player.GetStaminaNormalized() <= 0;
        //|| boss.GetHealthNormalized() <= 0;
        // TODO Add a next level instead of game over when boss dies

        public float GameTime() => elapsedTime;

        void Awake()
        {
            Instance = this;
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
            //boss = GameObject.FindGameObjectWithTag("Boss").GetComponent<Boss>();
        }
        void Start()
        {
            startTime = Time.time;
            stageCoin = 0;
            gameFinishUI.SetActive(false);
            gameOverUI.SetActive(false);
            StartCoroutine("scoring");
            AudioManager.Instance.Play("InGame");
            isFinishGame = false;

        }
        void Update()
        {
            if (! isFinishGame)
            {
                if (IsGameOver())
                {
                    if (gameOverUI.activeSelf == false)
                    {
                        gameOverUI.SetActive(true);
                    }
                    OnCompletedStage(null);
                }

                elapsedTime = Time.time - startTime;

            }
        }

        public void AddScore(float amount) => score += amount;
        IEnumerator scoring()
        {
            while (true)
            {
                score += 1 + playerController.GetPlayerSpeed() * 0.01f;
                if ((int)score % 10 == 0)
                    this.PostEvent(EventID.OnAchieveTargetScore, (int)score);
                if ((int)score % 5 == 0)
                    this.PostEvent(EventID.TriggerHeheEnemy, player.gameObject);
                yield return new WaitForSeconds(2.0f);
            }
        }
        public int GetScore() => (int)score;

        public void AddCoin(int amount)
        {
            stageCoin += amount;
            this.PostEvent(EventID.OnCoinCollect, stageCoin);
        }
        public int GetCoin
            () => stageCoin;

        void OnDestroy()
        {

            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnCompletedStage, (param) => OnCompletedStage((GameObject)param));
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnCompletedStage, (param) => OnCompletedStage((GameObject)param));
        }

        private void OnCompletedStage(GameObject param)
        {
            StopCoroutine("scoring");
            isFinishGame = true;
            StartCoroutine(OnFinishStage(param));
        }
        IEnumerator OnFinishStage(GameObject param)
        {   
            yield return new WaitForSeconds(restartTimer);
            FinishStageObjectTransfer objFinishStage = new FinishStageObjectTransfer(
                  score: (int)this.score,
                  coin: this.stageCoin,
                  elapsedTime: this.elapsedTime,
                  currentStage: this.currentScene,
                  nextStage: this.nextScene,
                  isCompleteStage: param ? true : false
            );
            gameOverUI.SetActive(false);
            gameFinishUI.SetActive(true);
            PlayerRef.Instance.AddCoin(stageCoin);
            PlayerRef.Instance.SaveCoin();
            stageCoin = 0;
            AudioManager.Instance.Play("WindTheme");
            this.PostEvent(EventID.OnFinishStageTrigger, objFinishStage);
        }
       
    }
}