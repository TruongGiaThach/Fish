using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    public class SharkCollider : MonoBehaviour
    {
        [SerializeField] int damage = 50;
        [SerializeField] GameObject explosionPrefab;

        private void OnTriggerEnter(Collider collision)
        {
            if (collision != null)
            {
                collision.GetComponent<Player>().TakeDamage(damage);
            }
            if (explosionPrefab != null)
            {
                Vector3 contact = collision.transform.position;
                var hitVFX = Instantiate(explosionPrefab, new Vector3(contact.x + (1 * Time.deltaTime), contact.y, 0), Quaternion.identity);
                DestroyParticleSystem(hitVFX);
            }
            Destroy(gameObject);
        }
        void DestroyParticleSystem(GameObject vfx)
        {
            var ps = vfx.GetComponent<ParticleSystem>();
            if (ps == null)
            {
                ps = vfx.GetComponentInChildren<ParticleSystem>();
            }
            Destroy(vfx, ps.main.duration);
        }

    }
}
