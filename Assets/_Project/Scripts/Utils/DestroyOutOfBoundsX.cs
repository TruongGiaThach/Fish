﻿using Shmup;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOutOfBoundsX : MonoBehaviour
{
    [SerializeField]Camera _camera;
    private float leftLimit = -35;
    private float bottomLimit = -4;
    private float topLimit = 4;

    SpawnerInterface _tower;
    public void SetSpawner(SpawnerInterface _spawnerInterface) => _tower = _spawnerInterface;
    public void SetCamera(Camera camera) => _camera = camera;
    void Update()
    {
        // Destroy dogs if x position less than left limit
        if (transform.position.x < leftLimit)
        {
            Destroy(gameObject);
        }
        // Destroy balls if y position is less than bottomLimit
        else if (transform.position.y < bottomLimit)
        {
            Destroy(gameObject);
        }
        else if (transform.position.y > topLimit)
        {
            _tower.Free(gameObject);
        }
        if (transform.position.x < _camera.ScreenToWorldPoint(new Vector3(0, 0, _camera.nearClipPlane)).x)
        {
            _tower.Free(gameObject);
        }
    }
}
