﻿using MEC;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections;
using UnityEngine;

namespace Shmup
{
    public class Player : Plane
    {
        [SerializeField] float maxStamina = 100;
        [SerializeField] float staminaConsumptionRate;

        [SerializeField] bool infiniteShield, isShieldActive;
        [SerializeField] int maxShield = 100, currentShield;
        [SerializeField] bool isImmortal = false;
        [SerializeField] float immortalTime = 0.5f;
        float stamina;

        void Start()
        {
            health = this.maxHealth;
            stamina = maxStamina;
            currentShield = 0;
            infiniteShield = false;
            isShieldActive = false;
            isImmortal = false;
        }

        public float GetStaminaNormalized() => stamina / maxStamina;
        public float GetShieldNormalized() => currentShield / (float)maxShield;

        void Update()
        {
            stamina -= staminaConsumptionRate * Time.deltaTime;
        }

        public void AddStamina(float amount)
        {
            stamina += amount;
            if (stamina > maxStamina)
            {
                stamina = maxStamina;
            }
        }
        public override void TakeDamage(int amount)
        {
            if (!isImmortal) { 
                if (isShieldActive)
                {
                    amount = DamageShield(amount);
                };
                DamageHealth(amount);
                AudioManager.Instance.PlayVfx("Land");
                StartCoroutine(TakeDamageCorotine());
            }
        }
        IEnumerator TakeDamageCorotine()
        {
            isImmortal = true;
            yield return new WaitForSeconds(immortalTime);
            isImmortal = false;
        }
        private void DamageHealth(int amount)
        {
            this.health -= amount;
            if (this.health <= 0)
            {
                Die();
            }
        }
        private int DamageShield(int amount)
        {
            int overDamage = 0;
            if (!infiniteShield)
            {
                if (currentShield  < amount)
                {
                    currentShield = 0;
                    overDamage = amount - currentShield;
                } else
                {
                    overDamage = 0;
                    currentShield -= amount;
                }
                if (currentShield < 1)
                {
                    isShieldActive = false;
                    this.PostEvent(EventID.OnShieldEnable, false);
                }
            }
            return overDamage;
        }

        public void AddShield(int amount, bool isInfiniteShield = false)
        {
            this.infiniteShield = isInfiniteShield;
         
            currentShield += amount;
            this.isShieldActive = currentShield > 0 ? true : false;
            this.PostEvent(EventID.OnShieldEnable, isShieldActive);
            if (currentShield > maxShield)
                currentShield = maxShield;
            this.PostEvent(EventID.OnShieldChange, currentShield);
        }
        protected override void Die()
        {
            // TODO: Implement VFX?  Freeze Controls?
        }
        void OnDestroy()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnAchieveTargetScore, (param) => OnAchieveTargetScore((int)param));
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnAchieveTargetScore, (param) => OnAchieveTargetScore((int)param));
        }
        void OnAchieveTargetScore(int score)
        {
            AddShield(15, false);
        }
    }
}