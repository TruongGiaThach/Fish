using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace Shmup
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] float speed = 5f;
        [SerializeField] float smoothness = 0.1f;
        [SerializeField] float leanAngle = 15f;
        [SerializeField] float leanSpeed = 5f;

        [SerializeField] GameObject model;

        [Header("Camera Bounds")]
        [SerializeField]
        Transform cameraFollow;

        [SerializeField] float minX = -8f;  //-2.8f
        [SerializeField] float maxX = 8f;       //2.8f
        [SerializeField] float minY = -4f;
        [SerializeField] float maxY = 4f;
        public float jumpSpeed;
        private Rigidbody rb;
        public Transform GroundCheck;
        public float CheckRadius;
        public LayerMask whatIsGround;
        public bool isGrounded;

        public int maxJumpValue;
        int maxJump;
        bool isJumping;

        InputReader input;
        Vector3 currentVelocity;
        Vector3 targetPosition;
        Player _player = null;
        public float GetPlayerSpeed() => speed;
        public void AddPlayerSpeed(float amount)
        {
            this.speed += amount;
            this.PostEvent(EventID.OnPlayerSpeedChange, this.speed);

        }
        public void AddPlayerJumpSpeed(float amount)
        {
            this.jumpSpeed += amount;
            //this.PostEvent(EventID.OnPlayerSpeedChange, this.speed);

        }
        void Start()
        {
            maxJump = maxJumpValue;
            rb = GetComponent<Rigidbody>();
            input = GetComponent<InputReader>();
            _player = gameObject.GetComponent<Player>();

        }

        //void Update()
        //{
        //    Collider[] hitColliders = Physics.OverlapSphere(GroundCheck.position, CheckRadius, whatIsGround);
        //    isGrounded = hitColliders.Length > 0 ? true : false;
        //    if (Input.GetMouseButtonDown(0) && maxJump > 0)
        //    {
        //        maxJump--;
        //        Jump();
        //    }
        //    else if (Input.GetMouseButtonDown(0) && maxJump == 0 && isGrounded)
        //    {
        //        Jump();
        //    }

        //    if (isGrounded)
        //    {
        //        maxJump = maxJumpValue;
        //    }
        //    if (isGrounded == false)
        //    {
        //    }

        //}
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                isJumping = true;
                AudioManager.Instance.PlayVfx("Jump");
            }
           

        }
        void LateUpdate()
        {
            var minPlayerY = cameraFollow.position.y + minY;
            var minPlayerX = cameraFollow.position.x + minX;
            if (transform.position.y < minPlayerY || transform.position.x < minPlayerX)
            {
                _player.TakeDamage(999);
            }
            //
            targetPosition = transform.position;
            // Calculate the min and max X and Y positions for the player based on the camera view
            minPlayerX = targetPosition.x;
            //cameraFollow.position.x + minX;
            var maxPlayerX = cameraFollow.position.x + maxX;
            minPlayerY = targetPosition.y;
            //cameraFollow.position.y + minY;
            var maxPlayerY = cameraFollow.position.y + maxY;

            // Clamp the player's position to the camera view
            targetPosition.x = Mathf.Clamp(targetPosition.x, minPlayerX, maxPlayerX);
            targetPosition.y = Mathf.Clamp(targetPosition.y, minPlayerY, maxPlayerY);

            // Lerp the player's position to the target position
            targetPosition += Vector3.right * (speed * Time.deltaTime);
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref currentVelocity, smoothness);
            transform.position = targetPosition;


        }
        private void FixedUpdate()
        {
            if (isJumping)
            {
                Jump();
                isJumping=false;
            }
        }
        void Jump()
        {

            rb.velocity = Vector3.zero;
            //transform.position += Vector3.right * (speed * Time.deltaTime);

            //rb.transform.position += new Vector3(speed, jumpSpeed, 0);
            //transform.position += Vector3.right * (10 * Time.deltaTime);
            rb.AddForce(Vector3.up *  jumpSpeed);

            //float gravity = Physics.gravity.magnitude;
            //float initialVelocity = CalculateJumpSpeed(1, gravity);

            //Vector3 direction = new Vector3(transform.position.x + 0.1f, transform.position.y + 1, transform.position.z) - transform.position;

            //rb.AddForce(initialVelocity * direction, ForceMode.Impulse);
        }

        private float CalculateJumpSpeed(float jumpHeight, float gravity)
        {
            return Mathf.Sqrt(2 * jumpHeight * gravity);
        }
        IEnumerator DestroySpeedBuff(float amount)
        {
            yield return new WaitForSeconds(0.5f);
            AddPlayerSpeed(-amount);
        }
        void OnDestroy()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnAchieveTargetScore, (param) => OnAchieveTargetScore((int)param));
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnAchieveTargetScore, (param) => OnAchieveTargetScore((int)param));

        }
        void OnAchieveTargetScore(int score)
        {
            AddPlayerSpeed(0f);
        }
    }
}