using Shmup.Assets._Project.Scripts.Event;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Shmup
{
    public class EndLevelCheckPoint : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            this.PostEvent(Assets._Project.Scripts.Models.EventID.OnEnterEndLevelLine, other.gameObject);
        }
    }
}
