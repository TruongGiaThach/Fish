using Shmup.Assets._Project.Scripts.Event;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    public class HeheEnemyCheckPoint : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            this.PostEvent(Assets._Project.Scripts.Models.EventID.TriggerHeheEnemy, other.gameObject);
        }
    }
}
