﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine;
using Utilities;

namespace Shmup
{
    public class StaticEnemy : MonoBehaviour
    {
        [SerializeField] GameObject explosionPrefab;
        [SerializeField, Layer] int layer = 8;

        private void OnCollisionEnter(Collision collision)
        {
            var player = collision.gameObject.GetComponent<Player>();
            if (player.GetShieldNormalized() > 0)
            {
                if (explosionPrefab != null)
                {
                    ContactPoint contact = collision.contacts[0];
                    Debug.Log(contact.point.z);
                    var hitVFX = Instantiate(explosionPrefab, new Vector3(contact.point.x + (1 * Time.deltaTime), contact.point.y, 0), Quaternion.identity);

                    DestroyParticleSystem(hitVFX);
                }

                player.TakeDamage(10);
                Destroy(gameObject);
            }
        }

        public UnityEvent OnSystemDestroyed;
        void DestroyParticleSystem(GameObject vfx)
        {
            var ps = vfx.GetComponent<ParticleSystem>();
            if (ps == null)
            {
                ps = vfx.GetComponentInChildren<ParticleSystem>();
            }
            Destroy(vfx, ps.main.duration);
        }
    }
}
