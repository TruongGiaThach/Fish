﻿using Eflatun.SceneReference;
using Shmup.Assets._Project.Scripts.Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Shmup {
    public class MainMenuUI : MonoBehaviour {
        [SerializeField] SceneReference startingLevel;
        [SerializeField] Button playButton;
        [SerializeField] Button quitButton;
        [SerializeField] TextMeshProUGUI coinText;
        private int playerCoin = 0;


        void Awake() {
            playButton.onClick.AddListener(() => Loader.Load(startingLevel));
            quitButton.onClick.AddListener(() => Helpers.QuitGame());
            Time.timeScale = 1f;
           
        }
        void Start()
        {
            playerCoin = PlayerRef.Instance.coin;
        }
        void Update()
        {
            coinText.text = $"{playerCoin}";
        }

    }
}