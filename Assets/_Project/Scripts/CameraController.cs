﻿using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using UnityEngine;

namespace Shmup
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] Transform player;
        [SerializeField] float speed = 2f;
        [SerializeField] GameObject ChangeStageLine;

        void Start()
        {
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
            this.ChangeStageLine.SetActive(false);

        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
            this.RegisterListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
            this.RegisterListener(EventID.OnAchieveTargetScore, (param) => OnAchieveTargetScore((int)param));

        }
        public void AddSpeed(float speed)
        {
            this.speed += speed;
            this.PostEvent(EventID.OnCameraSpeedChange, speed);
        }
        public float GetSpeed()
        {
            return this.speed ;
        }
        void OnEnterEndLevel(GameObject player)
        {
            if (player != null)
            {
                ChangeStageLine.SetActive(true);
                speed = 0;
            }
        }
        void OnPlayerSpeedChange(float speed)
        {
            this.speed = speed;
        }
        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnPlayerSpeedChange, (param) => OnPlayerSpeedChange((float)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnAchieveTargetScore, (param) => OnAchieveTargetScore((int)param));
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnEnterEndLevelLine, (param) => OnEnterEndLevel((GameObject)param));
        }
        void LateUpdate()
        {
            // Move the camera along the battlefield at a constant speed
            transform.position += Vector3.right * (speed * Time.deltaTime);
        }
        void OnAchieveTargetScore(int score)
        {
            //AddSpeed(1f);
        }
    }
}