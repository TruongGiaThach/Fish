﻿using System;
using UnityEngine;

namespace Shmup {
    public class Projectile : MonoBehaviour {
        [SerializeField] float speed;
        [SerializeField] GameObject muzzlePrefab;
        [SerializeField] GameObject hitPrefab;
        [SerializeField] int damage = 10;
        Transform parent;
        
        public void SetSpeed(float speed) => this.speed = speed;
        public void SetParent(Transform parent) => this.parent = parent;

        public Action Callback;
        
        void Start() {
            //if (muzzlePrefab != null) {
            //    var muzzleVFX = Instantiate(muzzlePrefab, transform.position, Quaternion.identity);
            //    muzzleVFX.transform.forward = gameObject.transform.forward;
            //    muzzleVFX.transform.SetParent(parent);
                
            //    DestroyParticleSystem(muzzleVFX);
                
            //}
        }
        
        void Update() {
            transform.SetParent(null);
            //TODO: Update later
            transform.position += transform.right * (speed * Time.deltaTime);
            
            Callback?.Invoke();
        }

        void OnCollisionEnter(Collision collision) {
            if (hitPrefab != null) {
                ContactPoint contact = collision.contacts[0];
                var hitVFX = Instantiate(hitPrefab, new Vector3( contact.point.x + (speed * Time.deltaTime), contact.point.y, contact.point.z), Quaternion.identity);
                
                DestroyParticleSystem(hitVFX);
            }

            var player = collision.gameObject.GetComponent<Player>();
            if (player != null)
            {
                player.TakeDamage(damage);
            }

            Destroy(gameObject);
        }
        
        //void DestroyParticleSystem(GameObject vfx) {
        //    var ps = vfx.GetComponent<ParticleSystem>();
        //    if (ps == null) {
        //        ps = vfx.GetComponentInChildren<ParticleSystem>();
        //    }
        //    Destroy(vfx, ps.main.duration);
        //}
        void DestroyParticleSystem(GameObject vfx)
        {
            if (vfx != null)
            {
                Destroy(vfx, 2.0f);
            }
        }
    }
}