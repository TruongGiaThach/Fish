﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Shmup.Assets._Project.Scripts.Models
{
    public class PlayerRef : MonoBehaviour
    {
        public float sfxVolume;
        public float musicVolume;
        public float dialogueVolume;
        public int score { get; private set; }
        public int coin { get; private set; }
        static PlayerRef s_instance;
        public static PlayerRef Instance
        {
            get
            {
                // instance not exist, then create new one
                if (s_instance == null)
                {
                    // create new Gameobject, and add EventDispatcher component
                    GameObject singletonObject = new GameObject();
                    s_instance = singletonObject.AddComponent<PlayerRef>();
                    singletonObject.name = "Singleton - PlayerRef";
                }
                return s_instance;
            }
            private set { }
        }

        public static bool HasInstance()
        {
            return s_instance != null;
        }
        void Awake()
        {
            // check if there's another instance already exist in scene
            if (s_instance != null && s_instance.GetInstanceID() != this.GetInstanceID())
            {
                // Destroy this instances because already exist the singleton of EventsDispatcer
                Common.Log("An instance of PlayerRef already exist : <{1}>, So destroy this instance : <{2}>!!", s_instance.name, name);
                Destroy(gameObject);
            }
            else
            {
                // set instance
                s_instance = this as PlayerRef;
                LoadSettings();
            }
        }


        void OnDestroy()
        {
            // reset this static var to null if it's the singleton instance
            if (s_instance == this)
            {
                s_instance = null;
            }
        }

        private void LoadSettings()
        {
            sfxVolume = PlayerPrefs.GetFloat("sfxVolume");
            musicVolume = PlayerPrefs.GetFloat("musicVolume");
            dialogueVolume = PlayerPrefs.GetFloat("dialogueVolume");
            score = PlayerPrefs.GetInt("score");
            coin = PlayerPrefs.GetInt("coin");
        }
        public void SaveSettings()
        {
            PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
            PlayerPrefs.SetFloat("musicVolume", musicVolume);
            PlayerPrefs.SetFloat("dialogueVolume", dialogueVolume);
            PlayerPrefs.Save();
        }
        public void AddScore(int score)
        {
            this.score += score;
        }
        public void SaveScore() {
            PlayerPrefs.SetInt("score", score);
            PlayerPrefs.Save();
        }
        public void AddCoin(int coin)
        {
            this.coin += coin;
        }
        public void SaveCoin()
        {
            PlayerPrefs.SetInt("coin", coin);
            PlayerPrefs.Save();
        }
    }
}
