﻿using Eflatun.SceneReference;
using UnityEngine;

namespace Shmup.Assets._Project.Scripts.Models
{

    public enum EventID
    {
        None = 0,
        OnPlayerHPChange,
        OnPlayerStaminaChange,
        OnPlayerSpeedChange,
        OnCameraSpeedChange,
        OnCoinCollect,
        OnSlowDebuffCollect,
        OnShieldItemCollect,
        OnShieldEnable,
        OnShieldChange,
        OnAudioStatusChange,
        OnAudioSourceChange,
        OnWallSpawn,
        OnEnterEndLevelLine,
        OnAchieveTargetScore,
        TriggerHeheEnemy,
        TriggerFlashScreen,
        OnFinishStageTrigger,
        OnCompletedStage,
    }
    public struct FinishStageObjectTransfer
    {
        public int score { get; private set; }
        public int coin { get; private set; }
        public float elapsedTime { get; private set; }
        public SceneReference currentStage { get; private set; }
        public SceneReference nextStage { get; private set; }
        public bool isCompleteStage { get; private set; }


        public FinishStageObjectTransfer(int score, int coin, float elapsedTime, SceneReference currentStage, SceneReference nextStage, bool isCompleteStage)
        {
            this.score = score;
            this.coin = coin;
            this.elapsedTime = elapsedTime;
            this.currentStage = currentStage;
            this.nextStage = nextStage;
            this.isCompleteStage = isCompleteStage;
        }
    }
}
