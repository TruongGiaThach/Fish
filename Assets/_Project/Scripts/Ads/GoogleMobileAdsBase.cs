﻿using GoogleMobileAds.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Shmup.Assets._Project.Scripts.Ads
{
    public class GoogleMobileAdsBase : MonoBehaviour
    {
        // These ad units are configured to always serve test ads.
#if UNITY_ANDROID
        protected const string _adUnitId = "";
#elif UNITY_IPHONE
        protected const string _adUnitId = "";
#else
        protected const string _adUnitId = "";
#endif

        protected InterstitialAd _interstitialAd;
        static GoogleMobileAdsBase s_instance;
        public static GoogleMobileAdsBase Instance
        {
            get
            {
                // instance not exist, then create new one
                if (s_instance == null)
                {
                    // create new Gameobject, and add EventDispatcher component
                    GameObject singletonObject = new GameObject();
                    s_instance = singletonObject.AddComponent<GoogleMobileAdsBase>();
                    singletonObject.name = "Singleton - GoogleMobileAdsBase";
                }
                return s_instance;
            }
            private set { }
        }

        public static bool HasInstance()
        {
            return s_instance != null;
        }

        void Awake()
        {
            // check if there's another instance already exist in scene
            if (s_instance != null && s_instance.GetInstanceID() != this.GetInstanceID())
            {
                // Destroy this instances because already exist the singleton of EventsDispatcer
                Common.Log("An instance of EventDispatcher already exist : <{1}>, So destroy this instance : <{2}>!!", s_instance.name, name);
                Destroy(gameObject);
            }
            else
            {
                // set instance
                s_instance = this as GoogleMobileAdsBase;
                // Initialize the Mobile Ads SDK.
                MobileAds.Initialize((initStatus) =>
                {
                    Dictionary<string, AdapterStatus> map = initStatus.getAdapterStatusMap();
                    foreach (KeyValuePair<string, AdapterStatus> keyValuePair in map)
                    {
                        string className = keyValuePair.Key;
                        AdapterStatus status = keyValuePair.Value;
                        switch (status.InitializationState)
                        {
                            case AdapterState.NotReady:
                                // The adapter initialization did not complete.
                                MonoBehaviour.print("Adapter: " + className + " not ready.");
                                break;
                            case AdapterState.Ready:
                                // The adapter was successfully initialized.
                                MonoBehaviour.print("Adapter: " + className + " is initialized.");
                                break;
                        }
                    }
                });
            }
        }
        public void Start()
        {
           
            LoadAd();
        }
        /// <summary>
        /// Loads the ad.
        /// </summary>
        public virtual void LoadAd()
        {


        }

        /// <summary>
        /// Shows the ad.
        /// </summary>
        public virtual  void ShowAd()
        {
        }

        /// <summary>
        /// Destroys the ad.
        /// </summary>
        public virtual void DestroyAd()
        {
        }

        /// <summary>
        /// Logs the ResponseInfo.
        /// </summary>
        public virtual void LogResponseInfo()
        {
        }

        protected virtual void RegisterEventHandlers(InterstitialAd ad)
        {
           
        }
    }
}
