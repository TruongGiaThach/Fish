using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Shmup
{
    [RequireComponent(typeof(Button))]
    public class AudioButton : MonoBehaviour
    {
        [SerializeField] bool isMute = false;
        [SerializeField] Sprite OffSprite;
        [SerializeField] Sprite OnSprite;
        Button mButton;
        void OnAudioStatusChange(bool isMute)
        {
            // change object sprite
            this.isMute = isMute;
            this.mButton.image.sprite = isMute ? OffSprite : OnSprite;
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnAudioStatusChange, (param) => OnAudioStatusChange((bool)param));

        }
        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnAudioStatusChange, (param) => OnAudioStatusChange((bool)param));
        }
        void Start()
        {
            mButton = GetComponent<Button>();
        //    isMute = AudioManager.Instance.IsMute();
            mButton.onClick.AddListener(TaskOnClick);
        }
        void TaskOnClick()
        {
            AudioManager.Instance.ChangeStatus(!isMute);
        }
    }
}
