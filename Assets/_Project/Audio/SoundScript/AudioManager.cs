﻿using UnityEngine;
using System;
using UnityEngine.Audio;
using UnityEngine.UI;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections.Generic;
using System.Linq;

namespace Shmup
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] public List<Sound> sounds;

        //public static AudioManager Instance;
        [SerializeField] private bool isMute;

        public static AudioManager Instance { get; private set; }

        [SerializeField] private Sound currentAudioSound;



        public void Awake()
        {
            isMute = false;
            if (Instance == null)
            {
                Instance = this as AudioManager;
                currentAudioSound = new Sound();
            }
            else if (Instance.GetInstanceID() != this.GetInstanceID())
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            foreach (Sound s in sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;
                s.source.volume = s.volume;
                s.source.pitch = s.pitch;
                s.source.loop = s.loop;
            }

        }

        private void Start()
        {
            //Play("Theme");
            Play("WindTheme");
        }

        public void PlayVfx(string vfx)
        {
            Sound s = sounds.Find( Sound => Sound.name == vfx);
            if (s == null)
            {
                Debug.Log("Sorry sound with name not found");
                return;
            }

            s.source.Play();
        }
        public void Play(string name)
        {
            if (currentAudioSound.source != null)
                currentAudioSound.source.Stop();

            Sound s = sounds.Find(Sound => Sound.name == name);

            if (s == null)
            {
                Debug.Log("Sorry sound with name t found");
                return;
            }

            s.source.Play();
            currentAudioSound = s;
        }

        public void Stop()
        {

            if (currentAudioSound.source != null)
            { currentAudioSound.source.Stop(); }

            currentAudioSound.source = null;
            // we are here to do a
        }
        public void ChangeStatus(bool _isMute)
        {
            this.isMute = _isMute;
            this.PostEvent(Shmup.Assets._Project.Scripts.Models.EventID.OnAudioStatusChange, _isMute);
        }
        public bool IsMute() => isMute;

    }

}
