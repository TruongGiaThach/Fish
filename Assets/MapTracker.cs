using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    public class MapTracker : MonoBehaviour
    {
        [SerializeField] private Transform _camera;
        [SerializeField] private Transform _map1;
        [SerializeField] private Transform _map2;
        private float _mapSize;
        private void Start()
        {
            _mapSize = _map1.GetComponent<BoxCollider>().size.x;
        }
        private void FixedUpdate()
        {
            if (_camera.position.x >= _map2.position.x)
            {
                _map1.position = new Vector3(_map2.position.x + _mapSize, _map1.position.y , _map1.position.z);
                SwitchMap();
            }
            if (_camera.position.x < _map1.position.x)
            {
                _map2.position = new Vector3(_map1.position.x - _mapSize, _map2.position.y , _map2.position.z);
                SwitchMap();
            }
        }

        private void SwitchMap()
        {
            Transform temp = _map1;
            _map1 = _map2;
            _map2 = temp;
        }
    }
}
