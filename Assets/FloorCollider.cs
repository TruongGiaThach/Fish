using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utilities;

namespace Shmup
{
    public class FloorCollider : MonoBehaviour
    {
        [SerializeField] GameObject explosionPrefab;
        [SerializeField, Layer] int layer = 8;

        private void OnCollisionEnter(Collision collision)
        {
            {
                var player = collision.gameObject.GetComponent<Player>();
                if (explosionPrefab != null)
                {
                    ContactPoint contact = collision.contacts[0];
                    var hitVFX = Instantiate(explosionPrefab, new Vector3(contact.point.x + (1 * Time.deltaTime), contact.point.y, 0), Quaternion.identity);

                    DestroyParticleSystem(hitVFX);
                }

                if (player != null)
                {
                    player.TakeDamage(1000);
                }

                Destroy(collision.gameObject);
            }
        }

        public UnityEvent OnSystemDestroyed;
        void DestroyParticleSystem(GameObject vfx)
        {
            var ps = vfx.GetComponent<ParticleSystem>();
            if (ps == null)
            {
                ps = vfx.GetComponentInChildren<ParticleSystem>();
            }
            Destroy(vfx, ps.main.duration);
        }
    }
}
