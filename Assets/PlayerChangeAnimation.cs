using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Shmup
{
    public class PlayerChangeAnimation : MonoBehaviour
    {
        Animator animator;
        private void Start()
        {
            animator = GetComponent<Animator>();
            animator.SetBool("isSlow", false);
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnSlowDebuffCollect, (param) => OnSlowDebuffCollect((int) param));
        }
        void OnSlowDebuffCollect(int duration)
        {
            animator.SetBool("isSlow", true);
            StartCoroutine(Timer(duration));
        }
        IEnumerator Timer(int duration)
        {
            yield return new WaitForSeconds(duration);
            animator.SetBool("isSlow", false);

        }
        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnSlowDebuffCollect, (param) => OnSlowDebuffCollect((int)param));
        }
    }
}
