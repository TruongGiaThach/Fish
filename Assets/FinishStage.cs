using Eflatun.SceneReference;
using Shmup.Assets._Project.Scripts.Event;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    public class FinishStage
        : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.activeSelf)
            {
                this.PostEvent(Assets._Project.Scripts.Models.EventID.OnCompletedStage, other.gameObject);
            }
        }
    }
}
