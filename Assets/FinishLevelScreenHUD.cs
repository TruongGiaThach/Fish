using Eflatun.SceneReference;
using Shmup.Assets._Project.Scripts.Ads;
using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Shmup
{
    public class FinishLevelScreenHUD : MonoBehaviour
    {
        [SerializeField] Button nextButton;
        [SerializeField] Button restartButton;
        [SerializeField] Button closeButton;
        [SerializeField] TextMeshProUGUI scoreText;
        [SerializeField] TextMeshProUGUI coinText;
        [SerializeField] TextMeshProUGUI elapsedTimeText;

        private int playerCoin;
        private int playerScore;
        private float playerElapsedTime;
        private bool isCompleteStage;
        SceneReference currentScene;
        SceneReference nextScene;
        [SerializeField] SceneReference mainMenuScene;
        void Start ()
        {
            nextButton.onClick.AddListener(() => Loader.Load(nextScene));
            restartButton.onClick.AddListener(() => Loader.Load(currentScene));
            closeButton.onClick.AddListener(() => Loader.Load(mainMenuScene));
        }
        private void OnEnable()
        {
            this.RegisterListener(Assets._Project.Scripts.Models.EventID.OnFinishStageTrigger,(param) => OnFinishStage((FinishStageObjectTransfer) param));
        }
        void OnFinishStage(FinishStageObjectTransfer obj)
        {
            InterstitialAdController.Instance.ShowAd();
            playerCoin = obj.coin;
            playerScore = obj.score;
            playerElapsedTime = obj.elapsedTime;
            currentScene = obj.currentStage;
            nextScene = obj.nextStage;
            isCompleteStage = obj.isCompleteStage;
        }
        private void Update()
        {
            scoreText.text = $"Score: {playerScore}";
            coinText.text = $"$ {playerCoin}";
            elapsedTimeText.text = $"Time: { TimeSpan.FromSeconds(playerElapsedTime).ToString(@"m\:ss")}";
            nextButton.interactable = (isCompleteStage)  ;
        }
        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(Assets._Project.Scripts.Models.EventID.TriggerFlashScreen, (param) => OnFinishStage((FinishStageObjectTransfer)param));

        }

    }
}
