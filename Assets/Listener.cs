using Shmup.Assets._Project.Scripts.Event;
using Shmup.Assets._Project.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    [RequireComponent(typeof(AudioListener))]
    public class Listener : MonoBehaviour
    {
        private AudioListener audioListener;
        void OnAudioStatusChange(bool isMute)
        {
            this.audioListener.enabled = !isMute;
        }
        private void OnEnable()
        {
            this.RegisterListener(EventID.OnAudioStatusChange, (param) => OnAudioStatusChange((bool)param));

        }
        private void OnDisable()
        {
            Assets._Project.Scripts.Event.EventDispatcher.Instance.RemoveListener(EventID.OnAudioStatusChange, (param) => OnAudioStatusChange((bool)param));
        }
        void Start()
        {
            audioListener = GetComponent<AudioListener>();
         //   this.audioListener.enabled = !AudioManager.Instance.IsMute();
        }
        
    }
}
