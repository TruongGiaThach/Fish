using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shmup
{
    public class PauseControl : MonoBehaviour
    {
        public GameObject SceneStart;
        private void Start()
        {
            Time.timeScale = 0;
            SceneStart.SetActive(true);
        }
        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Time.timeScale = 1;
                SceneStart.SetActive(false);
                this.gameObject.SetActive(false);
            }
        }
    }
}
